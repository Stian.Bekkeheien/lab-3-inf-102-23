package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    private int lowerbound;
    private int upperbound;

	@Override
    public int findNumber(RandomNumber number) {
        lowerbound = number.getLowerbound();
        upperbound = number.getUpperbound();

        while (lowerbound <= upperbound) {
            //Finds mid between lower- and higher-bound
            int mid = lowerbound + (upperbound - lowerbound) / 2;

            //Guesses mid
            int result = number.guess(mid);

            //Returns if correct
            if (result == 0) return mid;

            //Sets lowerbound to mid+1 if the number is higher than guess
            else if (result < 0) lowerbound = mid + 1;

            //Sets upperbound to mid-1 if guess is lower than mid
            else upperbound = mid - 1;

        }

        throw new IllegalStateException("Number not found within bounds!");
    }

}
