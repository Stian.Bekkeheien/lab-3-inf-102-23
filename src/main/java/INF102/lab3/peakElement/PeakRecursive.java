package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size();

        //Checks first number
        if (n == 1 || numbers.get(0) >= numbers.get(1)){
            return numbers.get(0);
        }

        //Checks last number
        if (numbers.get(n - 1) >= numbers.get(n - 2)){
            return numbers.get(n - 1);
        }


       return peakElementHelper(numbers, 0, numbers.size() - 1);
    }

    private int peakElementHelper(List<Integer> numbers, int low, int high){
        int mid = low + (high - low) / 2;

        //checks middle
        if (numbers.get(mid) >= numbers.get(mid - 1) && numbers.get(mid) >= numbers.get(mid + 1)) {
            return numbers.get(mid);
        }
        
        //Check if peak in left half
        if (mid > 0 && numbers.get(mid - 1) > numbers.get(mid)) {
            return peakElementHelper(numbers, low, mid - 1);
        }

        return peakElementHelper(numbers, mid + 1, high);
    }

}
