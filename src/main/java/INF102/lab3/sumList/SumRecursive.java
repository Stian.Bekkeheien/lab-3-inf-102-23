package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return sumHelper(list, list.size() - 1);
    }

    private static long sumHelper(List<Long> list, int length){
        if (length < 0){
            return 0;
        }
        return list.get(length) + sumHelper(list, length - 1);
    }
    

}
